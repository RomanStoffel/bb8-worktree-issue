package ch.mibex.gitworktree.reproduce.api;

import com.atlassian.bitbucket.repository.*;
import com.atlassian.bitbucket.scm.CommandOutputHandler;
import com.atlassian.bitbucket.scm.PushCommandParameters;
import com.atlassian.bitbucket.scm.Watchdog;
import com.atlassian.bitbucket.scm.bulk.BulkCommitsCommandParameters;
import com.atlassian.bitbucket.scm.bulk.BulkContentCommandParameters;
import com.atlassian.bitbucket.scm.git.GitScm;
import com.atlassian.bitbucket.scm.git.GitWorkTreeBuilderFactory;
import com.atlassian.bitbucket.scm.git.command.GitCommandBuilderFactory;
import com.atlassian.bitbucket.scm.git.command.GitScmCommandBuilder;
import com.atlassian.bitbucket.scm.git.worktree.GitCheckoutType;
import com.atlassian.bitbucket.scm.git.worktree.GitWorkTree;
import com.atlassian.bitbucket.scm.git.worktree.GitWorkTreeRepositoryHookInvoker;
import com.atlassian.bitbucket.scm.git.worktree.PublishGitWorkTreeParameters;
import com.atlassian.bitbucket.scm.ref.CreateBranchCommandParameters;
import com.atlassian.bitbucket.user.Person;
import com.google.common.io.ByteStreams;
import com.google.common.primitives.Bytes;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;

public class TestGitWorktree extends HttpServlet {
    private final RepositoryService repos;
    private final RefService refs;
    private final GitWorkTreeBuilderFactory worktrees;
    private final GitScm git;
    private final Person author = new Person() {
        @Nullable
        @Override
        public String getEmailAddress() {
            return "test@test.com";
        }

        @Nonnull
        @Override
        public String getName() {
            return "Test";
        }
    };

    public TestGitWorktree(RepositoryService repos, RefService refs, GitWorkTreeBuilderFactory worktrees, GitScm git) {
        this.repos = repos;
        this.refs = refs;
        this.worktrees = worktrees;
        this.git = git;
        System.out.println("===> hello world from plugin");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.getWriter().println("Hello world");
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String proj = req.getParameter("proj");
            String srcName = req.getParameter("source");
            String targetName = req.getParameter("target");

            Repository sourceRepo = repos.getBySlug(proj, srcName);
            Branch sourceBranch = refs.getDefaultBranch(sourceRepo);

            Repository targetRepo = repos.getBySlug(proj, targetName);
            if(false){
                // perfect! Works in BB 8 without push!
                GitScmCommandBuilder commands = git.getCommandBuilderFactory().builder(targetRepo);
                String result =  commands.fetch()
                        .repository(sourceRepo)
                        .refspec("+refs/heads/*:refs/heads/*")
                        .build(new SingleLineOutputHandler())
                        .call();
            }
            {
                worktrees.builder(targetRepo)
//                        .alternate(sourceRepo)
                        .checkoutType(GitCheckoutType.NONE)
                        .execute(worktree -> doGitOperations(worktree, sourceRepo, sourceBranch));
            }

            resp.getWriter().println("Done!");
        } catch (Exception e) {
            e.printStackTrace();
            resp.sendError(500);
        }
    }

    private Object doGitOperations(GitWorkTree workTree, Repository sourceRepo, Branch source) throws IOException {
        GitWorkTreeRepositoryHookInvoker invoker = new GitWorkTreeRepositoryHookInvoker() {
            @Override
            public boolean preUpdate(@Nonnull GitWorkTree gitWorkTree, @Nonnull List<RefChange> list) {
                return true;
            }

            @Override
            public void postUpdate(@Nonnull GitWorkTree gitWorkTree, @Nonnull List<RefChange> list) {

            }
        };
        String fetchResult =  workTree.builder()
                .fetch()
                .repository(sourceRepo)
                .refspec(source.getId())
                .build(new SingleLineOutputHandler())
                .call();
        String commitHash = workTree.builder()
                .author(author)
                .command("commit-tree")
                .argument(source.getLatestCommit() + "^{tree}")
                .argument("-m")
                .argument("initial commit")
                .build(new SingleLineOutputHandler())
                .call();

        workTree.builder()
                .updateRef()
                .set("HEAD", commitHash)
                .build()
                .call();

        String noExistingCommit = "0000000000000000000000000000000000000000";
        workTree.publish(new PublishGitWorkTreeParameters.Builder(invoker)
                .author(author)
                .branch("newly-published-branch", noExistingCommit)
                .build());
        return null;
    }

    private class SingleLineOutputHandler implements CommandOutputHandler<String> {
        private String result = "";

        @Nullable
        @Override
        public String getOutput() {
            return result;
        }

        @Override
        public void process(@Nonnull InputStream inputStream) throws IOException {
            byte[] bytes = ByteStreams.toByteArray(inputStream);
            result = new String(bytes, UTF_8);
        }

        @Override
        public void setWatchdog(@Nonnull Watchdog watchdog) {

        }
    }
}
